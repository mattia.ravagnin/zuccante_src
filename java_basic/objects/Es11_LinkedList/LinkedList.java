public class LinkedList {

    Node head; // head of list 

    public LinkedList(){
        head = null;
    }

    public LinkedList(int data){
        head = new Node(data);
    }

    public void insert(int data) { 
        Node node = new Node(data); 
        node.next = null; 
  
        // the empty list 
        if (head == null) { 
            head = node; 
        } 
        else {  
            Node last = head; 
            while (last.next != null) { 
                last = last.next; 
            }   
            // the last position
            last.next = node; 
        } 
    } 

    // remove the first element (if it is possible)
    public int remove(){
        if(head == null){
            return 666;
        }
        int ret = head.data;
        head = head.next;
        return ret;
    } 

    // remove the node in i position (ltr)
    public int remove(int i){
        if(i == 0) return remove(); // the same as remove()
        int ret = 666;
        Node pNode = head; // the previous node
        Node node = head.next; // we start from second node
        int j = 1;
        while(node != null && j < i){
            pNode = node;
            node = node.next;
            j++;
        }
        if (node == null) return ret;
        ret = node.data;
        pNode.next = (node != null) ? node.next : null;
        return ret;
    }

    public String toString(){
        String s = "head -> ";
        if (head == null){
            s += "null";
            return s;
        } 
        Node node = head;
        while(node.next != null){
            s += node.data + " -> ";
            node = node.next;
        }
        s += node.data + " -> null";
        return s;
    }
  
    // a nested class
    static class Node { 
        int data; 
        Node next; 
  
        Node(int d) { data = d; } 
    } 

    
}