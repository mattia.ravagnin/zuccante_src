# es_02

L'esempio è una modifica di quanto proposto in un articolo di Medium: [qui](https://codeburst.io/a-gentle-introduction-to-ajax-1e88e3db4e79); qui vediamo l'uso della proprietà `onreadystatechange` grazie a cui possiamo leggere lo stato della richiesta lavorando sempre ad **eventi**
```
if (request.readyState === 0) 
      console.log("UNSENT");
    if (request.readyState === 1) 
      console.log("OPENED");
    if (request.readyState === 2) 
      console.log("HEADER RECEIVD");
    if (request.readyState === 3) 
      console.log("LOADING");
    if (request.readyState === 4) {
      console.log("DONE")
```
