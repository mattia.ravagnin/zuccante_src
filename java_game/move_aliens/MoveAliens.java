import javax.swing.SwingUtilities;
import javax.swing.JFrame;

public class MoveAliens {
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> createAndShowGUI());
    }

    private static void createAndShowGUI() {
        System.out.println("Game started");
        JFrame f = new JFrame("Swing Paint Demo");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        f.add(new Board());
        f.pack();
        f.setVisible(true);
    } 

}
