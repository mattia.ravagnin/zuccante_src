import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Future08 {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor(r -> new Thread(r, "single-pool"));
        CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("f1 thread: " + Thread.currentThread().getName());
            return 666;
        }, executor);

        CompletableFuture<String> f2 = f1.thenApplyAsync(i -> {
            try {
                Thread.sleep(4000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("f1 thread: " + Thread.currentThread().getName());
            return i.toString();
        });

        try {
            System.out.println("f2 -> "+ f2.get());
        } catch (Exception e) {
            e.printStackTrace();
        }

        executor.shutdown();


    }

}
