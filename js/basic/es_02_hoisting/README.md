# Hoisting

Per comprendere `var` dobbiamo tenere a mente che JvaScript definisce sia a livello **global** che a livello **function** i **context execution** ([qui](https://hackernoon.com/execution-context-in-javascript-319dd72e8e2c) per approfondire); `var` è legata ad un context executione per le variabili `var` come pure per le funzioni abbiamo che (da [qui](https://medium.com/@leonardobrunolima/javascript-tips-var-let-const-and-hoisting-8ddf00aa47e))
> Hoisting is the way Javascript deal with variable (using the var keyword) and function declarations. 