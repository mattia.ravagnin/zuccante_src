const express = require('express')
const fs = require('fs')
const multer  = require('multer')

// app and middleware
const app = express();
app.use(express.static('uploads'));
app.set('view engine', 'ejs');

// let upload = multer({dest: 'uploads/'})

// SIMPLE: try to comment this block and uncomment the previous line
let myStorage = multer.diskStorage({
  destination: (req, file, next) => {
    next(null, 'uploads/')
  },
  filename: (req, file, next) => {
    next(null, file.originalname)
    // next(null, file.fieldname + '-' + Date.now() + '.jpeg')
  }
})

let upload = multer({ storage: myStorage })

// index.ejs in vews folder
app.get('/', (req, res) => {
  fs.readdir('uploads/', (err, files) => {
    /*
    files.forEach((file) => {
      console.log(`we have: ${file}`);
    });
    */
    console.log(files);
    res.render('index', {title: 'Gallery', photos: files});
  })
})

// upload view
app.get('/upload', (req, res) => {
  res.render('upload', {title: 'Umpload'})
})

app.post('/upload', upload.single('photo'), (req, res) => {
  console.log(`uploaded ${req.file.originalname}`);
  res.redirect('/');
})

// start the server
app.listen(3000, () => {
  console.log('A simple gallery\n Press Ctrl+C to terminate')
});