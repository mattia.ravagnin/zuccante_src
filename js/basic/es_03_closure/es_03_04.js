console.log(" ... un esempio di modulo")

function CoolModule() {

    let something = "cool"; 
    let another = [1, 2, 3];

    function doSomething() { 
        console.log(something);
    }
    
    function doAnother() {
        console.log(another.join("!"));
    }
    
    return {
        doSomething: doSomething, 
        doAnother: doAnother
    };
}

var foo = CoolModule(); // obtain module
foo.doSomething(); 
foo.doAnother(); 