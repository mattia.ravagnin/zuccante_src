const Es005 = {
    data() {
        return {
            memos: [{text: "primo"}, {text: "secondo"}]
        }
    },
    methods: {
        addMemo(text) {
            if (text.length == 0) return;
            this.memos.push({ text: text })
        },
        deleteAll() {
            this.memos = []
        }
    }
}

const app = Vue.createApp(Es005);

app.component('memo', {
    props: ['text'],
    methods: { },
    template:
        `<button>delete</button> : {{ text }}`
});

app.mount('#es005');
