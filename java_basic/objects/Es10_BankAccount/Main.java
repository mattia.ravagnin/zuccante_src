public class Main {

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Gioacchino Funciazza");
        sleep(2);
        b1.makeTransaction(BankAccount.Transaction.TRANSFER, 2000.0f);
        sleep(3);
        b1.makeTransaction(BankAccount.Transaction.WITHDRAWAL, 2000.0f);
        sleep(1);
        b1.makeTransaction(BankAccount.Transaction.WITHDRAWAL, 1000.0f);
        System.out.println("hostory of BankAccount n: " + b1.getNumber());
        for(BankAccount.Transaction t : b1.getHistory()) {
            System.out.print(t);
        }
        System.out.println("balance: " + b1.getBalance());
        b1.printHistory();
    }

    public static void sleep(int sec) {
        try{
            Thread.sleep(sec*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }  
    }

}

