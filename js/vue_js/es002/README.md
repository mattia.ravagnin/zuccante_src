# es002

Il nostro secondo esempio si articola: una lista di memo. Qui sotto alcuni riferimenti alla guida introduttiva.

- *list rendering* [qui](https://v3.vuejs.org/guide/list.html#mapping-an-array-to-elements-with-v-for).
- *form input binding* [qui](https://v3.vuejs.org/guide/forms.html).
- *conditional rendering* [qui](https://v3.vuejs.org/guide/conditional.html).
- *event handling* [qui](https://v3.vuejs.org/guide/events.html).