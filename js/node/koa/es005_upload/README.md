# upload

Qui vediamo come caricare dei file sul server.

## body

- `multipart {Boolean}` Parse multipart bodies, default false
- `urlencoded {Boolean}` Parse urlencoded bodies, default `true`
- `formidable {Object}` Options to pass to the formidable multipart parser

**formidable** è un modulo usato, [qui](https://www.npmjs.com/package/formidable), che permette varie opzioni qui passate attraverso la proprietà `formidable`

- `options.uploadDir {string}` - default `os.tmpdir()`; the directory for placing file uploads in. You can move them later by using fs.rename().
- `options.keepExtensions {boolean}` - default `false`; to include the extensions of the original files or not

Qui manteniamo le estensioni anche se, rinominando i file col loro nome originario l'opzione a `true` non è necessaria.

## fs module

`fs` è un vasto modulo di Node, [qui](https://nodejs.org/api/fs.html#fs_fs_readdir_path_options_callback) la sua home page. Noi useremo due metodi asincroni.  
Per rinominare il file appena caricato in modo da non mantenere la sola estensione (vedi sopra e tralasciando la sovrascrittura), usaimo [fs.rename](https://nodejs.org/api/fs.html#fs_fs_rename_oldpath_newpath_callback)
``` javascript
fs.rename('oldFile.txt', 'newFile.txt', (err) => {
  if (err) throw err;
  console.log('Rename complete!');
});
```
per leggere il contenuto di una directory usaiamo [fs.readdir](https://nodejs.org/api/fs.html#fs_fs_readdir_path_options_callback)
``` jacascript
fs.readdir(path, (err, items) => {
    items.forEach((item) => { ... }
});
```