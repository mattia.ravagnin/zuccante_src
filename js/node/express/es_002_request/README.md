# es_002_request

Nell'esempio precedente abbiamo già usaro l'oggetto `req` che incapsuola la *request* HTTP, qui approfondiamo la faccenda, vedi [qui](https://expressjs.com/en/4x/api.html#req.body).

## req.body

> Contains key-value pairs of data submitted in the request body. By default, it is undefined, and is populated when you use body-parsing middleware such as `express.json()` or `express.urlencoded()`.

ad esempio (tratto dalla documentazione)
``` javascript
var express = require('express')

var app = express()

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.post('/profile', function (req, res, next) {
  console.log(req.body)
  res.json(req.body)
})
```
Nel nostro esempio effettueremo il test usando
```
curl -H "Content-Type: application/json" -d '{"data": "buzz"}' localhost:3000/form
```

## routing

Quindi proviamo le altre possibilità di *routing*:
``` javascript
app.get('/', (req, res) => {  
    res.json({ msg : 'GET request, /'});
});

app.post('/', (req, res) => {
    res.json({ msg : 'POST request, /'});
});

app.put('/', (req, res) => {
    res.json({ msg : 'PUT request, / '});
});

app.delete("/", (req, res) => {
    res.json({ msg : 'DELETE request, /'});
});
```