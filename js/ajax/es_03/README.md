# il server node

Per partire  col nostro primo server node dobbiamo capire come node lavora con e gestisce gli **eventi**, un buon punto di partenza lo si può trovare [qui](https://nodejs.org/api/events.html) nella documentazione ufficiale. 
```
const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event', () => {
  console.log('an event occurred!');
});
myEmitter.emit('event');
```
altro esempio
```
req.on('data', (chunk) => {
      body += chunk
      console.log('Partial body: ' + body)
    })
```
Mediante `on()` possiamo aggiungere una **listener function** che attende l'emissione dell'evento - in questo caso il dato inviato - in modo, tipicamente, da poterlo gestire. Ora `req` è un oggetto della classe `http.ClientRequest`, [qui](https://nodejs.org/dist/latest-v11.x/docs/api/http.html#http_class_http_clientrequest) per le API, esso è uno `Stream`, [qui](https://nodejs.org/api/stream.html) per le API, ed ogni `Stream` è a sua volta un `EventEmitter`. Node permette di lavorare con i *binary data* usando i `Buffer`, [qui](https://nodejs.org/dist/latest-v11.x/docs/api/buffer.html), gli `Stream` comunicano con un buffer.
Un articolo interessante, da accompagnare con le API di node, è il seguente 
> "Events and Streams in Node.js" di Meet Zaveri [qui](https://codeburst.io/basics-of-events-streams-and-pipe-in-node-js-b84578c2f1be).

