# es004_crude_hibernate

L'esempio che qui proponiamo è tratto, addomesticato ed aggiornato da [qui](https://www.dariawan.com/tutorials/spring/spring-boot-mustache-crud-example/).
Spring Boot offre poi delle ottime guide, consigliamo di affiancare la lettura di
- **Accessing data with MySQL** [qui](https://spring.io/guides/gs/accessing-data-mysql/).

## i moduli

Partendo da [qui](https://start.spring.io/) scegliamo i moduli per un progetto *Gradle*

- **Spring Web** per il REST WEB completo.
- **Mustache** per i template
- **Spring Data JPA** è il cuore del CRUD, usa Hibernate  
- **MariDB Driver**
- **Lombok**
- **DevTools**

## creazione del DB su MariaDB

```
CREATE DATABASE es004SB;

GRANT ALL ON es004SB.* to 'es004SB'@localhost IDENTIFIED BY 'zuccante@2021';

SHOW GRANTS FOR 'es004SB'@localhost;
```
si può quindi `mysql -u es004SB -p`.

## le impostazioni per il DB

In `application.properties` impostiamo driver e parametri per il DB
```
# DB
spring.datasource.url=jdbc:mariadb://localhost:3306/es004SB
spring.datasource.username=es004SB
spring.datasource.password=zuccante@2021
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
```

## hibernate

Qui sotto lo schema con cui lavora Hibernate  
![grafo](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/images/architecture/data_access_layers.svg).

> **NOTA** Per vedere come *Hibernate* mappa i tipi di dati [qui](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#basic).

Hibernate è in grado di generare lo schema a partire dalla *annotation*, [qui](https://docs.jboss.org/hibernate/orm/6.0/userguide/html_single/Hibernate_User_Guide.html#configurations-hbmddl)
per i dettagli, in particolare
```
spring.jpa.hibernate.ddl-auto=
```
- `none`: nessuna medifica allo schema **default**
- `update`: Hibernate aggiorna tenendo conto dello schema definito nel *model* tramite le *annotation*
- `create`: crea il DB ma non lo cancella alla chiusura dell'applicazione
- `create-drop`: come sopra solo che il DB viene cancellato ala fine della sessione.
Noi poniamo
```
# hibernate
spring.jpa.hibernate.ddl-auto=create-drop
// spring.jpa.hibernate.use-new-id-generator-mappings=false
```
Del primo abbiamo già detto, per il secondo si veda [qui](https://docs.jboss.org/hibernate/orm/6.0/userguide/html_single/Hibernate_User_Guide.html#identifiers-generators-auto): si usa il sottostante motore del DB per generare
il nuovo `id` senza creare una tabella di appoggio. Per `@Column` e le sue opzioni vedasi [qui](https://docs.jboss.org/hibernate/jpa/2.1/api/javax/persistence/Column.html).

## model

E' la classe `User` che, come detto, rappresenta il dato che viene salvato nel DB in modo **persistente**: in particlare
`@Getter @Setter` ci forniscono gratis i metodi *setter* e *getter* per tutti i campi `private`.
```java
@Entity
@Getter @Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    @NotNull
    @Column(unique = true)
    private String email;

    private String phone;

    private String address;
}
```

## repository

L'annotation `@Repository`

E' l'interfaccia per il CRUS, vedi [qui](https://docs.spring.io/spring-data/data-commons/docs/current/api/org/springframework/data/repository/PagingAndSortingRepository.html): `PagingAndSortingRepository` estende
appunto `CrudRepository`. Una volta impostata l'interfaccia SPring la implementa per creare il CRUD
> This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
CRUD refers Create, Read, Update, Delete.


### `CrudRepository<T,ID>`

Partiamo con l'*annotationé `@Repository`, è la principale interfaccia per il CRUD, [qui](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html) per le API,
in realtà poco usata direttamente preferendo la sua estensione che presenteremo qui di seguito. Ecco i suoi metodi
- `long count()`
- `void delete(T entity)`
- `void deleteAll()`
- `void deleteAll(Iterable<? extends T> entities)`
- `void deleteById(ID id)`
- `boolean existsById(ID id)`
- `Iterable<T> findAll()`
- `Iterable<T> findAllById(Iterable<ID> ids)`
- `Optional<T> 	findById(ID id)`
- `<S extends T> S save(S entity)`: ritorna anche l'*entity*. 
- `<S extends T> Iterable<S> saveAll(Iterable<S> entities)`

### `PagingAndSortingRepository<T,ID>`

E' l'interfaccia principale per il REST, [qui](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/PagingAndSortingRepository.html).
Espone i seguenti metodi che vanno ad integrare la precedente.
- `Page<T> findAll(Pageable pageable)`
- `Iterable<T> findAll(Sort sort)`
`Page` è una *sublist* che offre altre possibilità, [qui](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/domain/Page.html) per le API,
  `Pageable` , [qui](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/domain/Pageable.html) per le API,
  dà le informazioni per la *pagination*. `Sort`, [qui](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/domain/Sort.html) per le API, dà indicazioni sull'ordinamento.

### `JpaSpecificationExecutor<T>`

Utile per **query aggregate** e non solo, [qui](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaSpecificationExecutor.html)
per le API

Ha i metodi
- `long count(Specification<T> spec)`
- `findAll(Specification<T> spec)`
- `Page<T> findAll(Specification<T> spec, Pageable pageable)`
- `List<T> findAll(Specification<T> spec, Sort sort)`
- `Optional<T> findOne(Specification<T> spec)`

`Specification`  [qui](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/domain/Specification.html) per le API, è utile per articolare la clausola `WHERE` con operatori logici ed altro.

## service

Il *repository* offre i metodi standard, qui li aggiustiamo ai nostri fini realizzando la **business logic**
```java
public User findById(Long id) {
    return userRepository.findById(id).orElse(null);
}
```
o ritorna un `User` o `null`, vedasi classe `Optional` di Java. Ora soffermiamoci su
``` java
Page<T> findAll(Pageable pageable)
```
`Pageable` è un interfaccia e per creare un oggetto di tipo `Pageable` usiamo `PageRequest`, [qui](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/domain/PageRequest.html) per le API.
Usiamo in particolare
```java
public static PageRequest of(int page, int size, Sort sort)
```
Quindi a partire da una "pagina" ritorniamo una lista
```java
public List<User> findAll(int pageNumber, int rowPerPage) {
    List<User> users = new ArrayList<>();
    Pageable sortedByLastUpdateDesc = PageRequest.of(pageNumber -1, rowPerPage, Sort.by("id").ascending());
    userRepository.findAll(sortedByLastUpdateDesc).forEach(users::add);
    return users;
}
```

## controller

Inizia con
```java
@Controller
```
usato per pagine WEB e *rendering* con *template*, nel nostro caso *mustache*, ma non
```java
@RestController
```
usato per il rest in `json` o `XML` (lo abbiamo comunque visto all'opera con `ModelAndView` (vedi primo esempio coi *template*). Qui entriamo nel vivo, come già visto con gli altri esempi.
```java
@Value("${msg.title}")
private String title;
```
accediamo ad `application.properties`, veniamo quindi al `CRUD HTTP`
```java
@GetMapping(value = {"/", "/index"})
    public String index(Model model) {
    model.addAttribute("title", title);
    return "index";
}
```
abbiamo usato al posto dell'interfaccia`Model`, [qui](https://docs.spring.io/spring-framework/docs/5.3.10-SNAPSHOT/javadoc-api/org/springframework/ui/Model.html) per la documentazione, a suo tempo (esercizio precedente) la classe `Map<String, Object>`. Segue la visualizzazione più raffinata, quella con la paginazione
```java
// GET
    @GetMapping(value = "/users")
    public String getUsers(Model model,
        @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
    List<User> users = userService.findAll(pageNumber, ROW_PER_PAGE);
    long count = userService.count();
    boolean hasPrev = pageNumber > 1;
    boolean hasNext = ((long) pageNumber * ROW_PER_PAGE) < count;
    model.addAttribute("users", users);
    model.addAttribute("hasPrev", hasPrev);
    model.addAttribute("prev", pageNumber - 1);
    model.addAttribute("hasNext", hasNext);
    model.addAttribute("next", pageNumber + 1);
    return "user-list";
}
```
cui corrisponde il *template* ritornato come stringa: usiamo `@Controller` e non `@RestController`. 
Per tirare fuori il singolo
```java
// GET
@GetMapping(value = "/users/{userId}")
public String getUserById(Model model, @PathVariable long userId) {
    User user = null;
    try {
        user = userService.findById(userId);
        model.addAttribute("allowDelete", false);
    } catch (Exception ex) {
        model.addAttribute("errorMessage", ex.getMessage());
    }
    model.addAttribute("user", user);
    return "user";
}
```
Veniamo quindi all'aggiunto di un utente mediante un `POST`
```java
// POST
@PostMapping(value = "/users/add")
public String addUser(Model model,
@ModelAttribute("user") User user) {
    try {
        User newUser = userService.save(user);
        return "redirect:/users/" + String.valueOf(newUser.getId());
    } catch (Exception ex) {
        // log exception first,
        // then show error
        String errorMessage = ex.getMessage();
        logger.error(errorMessage);
        model.addAttribute("errorMessage", errorMessage);
        model.addAttribute("add", true);
        return "user-edit";
    }
}
```
Qui è interessante l'uso di `@ModelAttribute("user") User user` che permette, nel `Model` di associare già da subito
l'attributo `user` all'oggetto passato, `POST`, dal form (chiaramente se tutto va a buon fine). Per le API [qui](https://docs.spring.io/spring-framework/docs/current/javadoc-api/).
- `@modelAttribute`   per catturare il parametri di un form
- `@RequestBody` per catturare l'intera richiesta (utile più nei REST `json`)
Per quanto segue non c'è molto da dire, soffermiamoci solo sul fatto che **update** lo facciamo con un `POST`
```java
 // POST
@PostMapping(value = {"/users/{userId}/edit"})
public String updateUser(Model model, @PathVariable long userId, @ModelAttribute("user") User user) {
    try {
        user.setId(userId);
        userService.update(user);
        return "redirect:/users/" + String.valueOf(user.getId());
    } catch (Exception ex) {
        // log exception first,
        // then show error
        String errorMessage = ex.getMessage();
        logger.error(errorMessage);
        model.addAttribute("errorMessage", errorMessage);
        model.addAttribute("add", false);
        return "user-edit";
    }
}
```

ed il **delete** lo facciamo ancora con un `POST`
```java
// POST
@PostMapping(value = {"/users/{userId}/delete"})
public String deleteUserById(Model model, @PathVariable long userId) {
    try {
        userService.deleteById(userId);
        return "redirect:/users";
    } catch (Exception ex) {
        String errorMessage = ex.getMessage();
        logger.error(errorMessage);
        model.addAttribute("errorMessage", errorMessage);
        return "user";
    }
}
```

