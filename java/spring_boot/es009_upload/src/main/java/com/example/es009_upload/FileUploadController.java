package com.example.es009_upload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.stream.Collectors;

@Controller
public class FileUploadController {

    FileStorageService service;

    @Autowired
    public FileUploadController(FileStorageService service) {
        this.service = service;
    }

    @GetMapping("/")
    String showForm(Model model) {
        model.addAttribute("files", service.loadAll().map(
                path -> new FileInfo(path.getFileName().toString(), "files/" + path.getFileName().toString()))
                        .collect(Collectors.toList()));
        return "uploadForm";
    }


    @PostMapping("/")
    String uploadFile(RedirectAttributes ra, @RequestParam(value = "myFile") MultipartFile multipartFile) throws IOException {
        try{
            service.save(multipartFile);
            ra.addFlashAttribute("message", "uploaded file" + multipartFile.getOriginalFilename());
            return "redirect:/";
        } catch (RuntimeException e) {
            ra.addFlashAttribute("message", "no file");
            return "redirect:/";
        }
    }

    @GetMapping("/files/{filename:.+}")
    //  @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = service.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

}
