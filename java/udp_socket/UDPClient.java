import java.io.*;
import java.net.*;

class UDPClient {
    public static void main(String args[]){
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
         
        byte[] sendData = null;
        byte[] receiveData = null;
         
        try {
            InetAddress ipAddress = InetAddress.getByName("localhost");
            DatagramSocket clientSocket = new DatagramSocket(); 
            while(true) {
                sendData = new byte[1024];
                receiveData = new byte[1024];
                String sentence = inFromUser.readLine();
                sendData = sentence.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, 9876);
                clientSocket.send(sendPacket);
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);
                String modifiedSentence = new String(receivePacket.getData());
                System.out.println("FROM SERVER: " + modifiedSentence);
            }
        } catch(UnknownHostException uhe){
            uhe.printStackTrace();
        } catch(SocketException se){
            se.printStackTrace();
        } catch(IOException ioe){
            ioe.printStackTrace();
        } 
    }
}
