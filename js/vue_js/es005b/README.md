# es005b

Continuando l'esempio precedente mettiamo all'opera la cancellazione: in questa soluzione lavoriamo in modo sincrono accedendo a proprietà paterne mediante `$parent`. QUi sotto il metodo di cancellazione
```js
deleteMemo(index) {
    this.$parent.memos.splice(index, 1);
}
```