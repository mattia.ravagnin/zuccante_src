package com.example.es002_restfull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es002RestfullApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es002RestfullApplication.class, args);
	}

}
