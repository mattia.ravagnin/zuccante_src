package com.example.es013_authentication_jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es013AuthenticationJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es013AuthenticationJpaApplication.class, args);
	}

}
