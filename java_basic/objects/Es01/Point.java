import java.lang.Math;

public class Point {
    
    public double x, y;
    
    public static Point origin = new Point();
    
    public double modulus () { return Math.sqrt(x*x+y*y); };
    
    public Point sub (Point p) {
        Point r = new Point();
        r.x = this.x - p.x; // x - p.x; 
        r.y = this.y - p.y; // y - p.y;
        return r;
    }
    
    public double distance (Point p) {
        Point d = this.sub(p); // Point d = this.sub(p);
        return d.modulus();
    }
    
}
