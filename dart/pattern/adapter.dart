const adapteeMessage = 'Adaptee#method was called';

// An adapter allows two incompatible interfaces to work together

// first interface
class Adaptee {
  String method() {
    print('Adaptee#method is being called');
    return adapteeMessage;
  }
}

// second interface
abstract class Target {
  String call();
}

class Adapter implements Target {
  String call() {
    var adaptee = Adaptee();
    print('Adapter#call is being called');
    return adaptee.method();
  }
}

void main() {
  var adapter = Adapter();
  var result = adapter.call();
  identical(result, adapteeMessage) ? print("YES") : print("NO");
}
