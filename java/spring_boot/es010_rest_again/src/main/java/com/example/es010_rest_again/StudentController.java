package com.example.es010_rest_again;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    // GET
    @GetMapping(path = {"/", "/students"})
    public List<Student> getAllUsers() {
        return studentService.getStudents();
    }

    // POST
    @PostMapping
    public void addStudent(@RequestBody Student student){
        studentService.addNewStudent(student);
    }

    // PUT
    @PutMapping("{studentId}")
    public void updateStudent(@PathVariable("studentId") Long id,
                              @RequestParam(required = false) String name,
                              @RequestParam(required = false) String email
    ){
        studentService.updateStudent(id, name, email);
    }

    // DELETE
    @DeleteMapping("{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long id) {
        studentService.deleteStudent(id);

    }
}
