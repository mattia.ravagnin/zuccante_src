package com.example.es003_template.controller;

import com.example.es003_template.model.Book;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class BookController {

    private static final Random rnd = new Random();

    @GetMapping("/books")
    public ModelAndView getProducts(Map<String, Object> model){

        List<Book> bookList = IntStream.range(0,7)
                .mapToObj(this::getBook)
                .collect(Collectors.toList());

        model.put("bookList", bookList);
        return new ModelAndView("books", model);
    }

    // create a book
    private Book getBook(int i){
        return new Book(i,
                "ISBN:" + (i + 600),
                "Book Name " + (i + rnd.nextInt(10)),
                "Author " + i,
                (double) (100 * (i + rnd.nextInt(10))));
    }
}
