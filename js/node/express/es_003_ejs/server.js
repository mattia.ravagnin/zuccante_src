const express = require('express');
const app = express();

// set view engine
app.set('view engine', 'ejs');

let list =
  [
    {goal: 'alpha'},
    {goal: 'beta'},
    {goal: 'gamma'},
    {goal: 'delta'}, 
    {goal: 'epsilon'}, 
  ];


app.get('/', function(req, res){
  res.render('index',
    {title: "My Expres App", user: "Paolino Paperino", todos: list}
  );
});

app.listen(3000, () => console.log('Server ready'))

