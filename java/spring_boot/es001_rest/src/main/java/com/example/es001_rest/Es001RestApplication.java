package com.example.es001_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es001RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es001RestApplication.class, args);
	}

}
