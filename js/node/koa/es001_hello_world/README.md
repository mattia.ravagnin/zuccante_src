# Hello world using middleeare

Ricordando che `await` mette in pasua la funzione (asincrona) fintantò che il `promise` ha dato il suo valore (la computazione asincrona è terminata), [qui](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await) per il ripasso. Lo schema dei *middleware* risulta più leggibile che coi *callback*, ecco la sequenza della chiamate
```
logger =>  x-response-time => logger => response
```
quando `response` si trova in cima allo *stack*, non essendoci altri `nect` si procede con l'esecuzione facendo il *pop*.

### context

In `Express` abbiamo `req` e `res`, qui abbiamo solo `ctx`.
``` javascript
app.use(async ctx => {
  ctx; // is the Context
  ctx.request; // is a Koa Request
  ctx.response; // is a Koa Response
});
```
L'accesso ai grezzi `req` e`res` di node si fa con `ctx.req` e `ctx.res`. L'oggetto `app.context` che troviamo come argomento `ctx` della computazione asincrona, la *function* preceduta da `await`, ecco come può essere usato (dalla documentazione ufficiale) aggiungendo altre proprietà
``` javascript
app.context.db = db();

app.use(async ctx => {
  console.log(ctx.db);
});
```
