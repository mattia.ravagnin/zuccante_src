const Sequelize = require('sequelize');

let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
  host: 'localhost',
  dialect: 'mariadb',
  pool: {
    max: 5, 
    min: 0, 
    idle: 10000 
  },
});

let User = sequelize.define('user', {
  firstName: {
    type: Sequelize.STRING,
    field: 'first_name' 
  },
  lastName: {
    type: Sequelize.STRING
  }},
  { freezeTableName: true }
);


// Note: using `force: true` will drop the table if it already exists
User.sync({force: true}).then(() => {
  return User.create({
    firstName: 'John',
    lastName: 'Hancock'
  });
}).then(() => {
  User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
  });
});

// sequelize.close();