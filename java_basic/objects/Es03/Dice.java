import java.util.Random;

public class Dice {   
    
    int upperFace;
    static Random rnd = new Random(System.currentTimeMillis);
    
    Dice(int n){
        upperFace = n;
    }
    
    void roll() {
        upperFace = 1 + rnd.nextInt(6);
    }
    
}
