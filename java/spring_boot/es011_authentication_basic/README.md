#es011_authentication_basic

Questo è il primo esempio di autenticazione tratto da una serie di tutorial online, con mie modifiche e commenti; 
"Basic Authentication": [qui](https://www.techgeeknext.com/spring-boot-security/basic_authentication_web_security) l'originale.

## creazione progetto

- Spring Web
- Spring Boot DevTools
- Spring Security

## spring security

Per la documentazione esiste un amplio *reference* [qui](https://docs.spring.io/spring-security/site/docs/3.2.0.RC2/reference/htmlsingle/#jc).

## il file di configurazione

Prima di tutto un semplice esempio di configurazione *in memory* (cioè non lasciando al file `application.properties`)
```java
Configuration
 @EnableWebSecurity
 public class HttpBasicSecurityConfig extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
                http.authorizeRequests().antMatchers("/**").hasRole("USER").and().httpBasic();
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
                auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
        }
 }
```
- nel primo metodo definiamo un **filtro**
- nel secondo i **ruoli** (account) filtrati
Nel nostro caso in `application.properties` (in alternativa un file `yaml` come nell'esempio originale)
```
spring.security.user.name=user
spring.security.user.password=zuccante@2021
spring.security.user.roles=USER
```  
Riprendiamo il codice
```java
 http.authorizeRequests()
        .anyRequest().authenticated()
        .and().httpBasic();
```
- `HTTPSecurity` [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/builders/HttpSecurity.html)
ci permette di lavorare un modo è un *builder*, corrisponde nel vecchio XML a `<http>...</http>`, fra l'alro implementa `HttpSecurityBuilder<HttpSecurity>`
- `autorizationRequests()`: per gestire gli accessi in modo ristretto.
- `anyRequest()`: ogni richiesta (un primo filtro).
- `ExpressionUrlAuthorizationConfigurer.AuthorizedUrl`: [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/configurers/ExpressionUrlAuthorizationConfigurer.AuthorizedUrl.html).
contiene varie possibilità fra cui `authenticated()`.
- `httpBasic()`: [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/builders/HttpSecurity.html#httpBasic()): HTTP basic, alla documentazione per un esemòpio.

## la codifica delle credenziali

Base64 permette di codificare binario in ASCII ed è molto usata nel WEB vedi Wikipedia(en) [qui](https://en.wikipedia.org/wiki/Base64).
Quindi qui tutto viene passato **in chiaro** (per ovvi motivi didattici). Sul terminale LINUX
```
genji@P330:~$ echo -n 'user:zuccante@2021' | base64
dXNlcjp6dWNjYW50ZUAyMDIx
```
che ritroviamo fra gli *header* (TAB network su devtools Mozilla)
```
Authorization Basic dXNlcjp6dWNjYW50ZUAyMDIx
```
[qui](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization) dettagli sull'*headrer*.


## curl

Riportiamo qui di seguito la sintassi di `curl`
```
curl --user username:password https://example.com
```

## logout

In questo esempio non è possibile!

## materiali

[1] "Spring Security Java Config Preview: Web Security", blog[qui](https://spring.io/blog/2013/07/03/spring-security-java-config-preview-web-security).  
[2] [https://www.base64decode.org](https://www.base64decode.org/)-