import 'dart:async';

void main() {
  Future<int> getNumberDelayed(int i) =>
      Future.delayed(Duration(seconds: i), () => i);
  List<Future<int>> futures = <Future<int>>[];
  for (int i = 1; i <= 10; i++) {
    futures.add(getNumberDelayed(i));
  }
  Stream<int> stream = streamFromFutures(futures);
  stream.listen((data) => print('yeld: $data'));
}

Stream<T> streamFromFutures<T>(Iterable<Future<T>> futures) async* {
  for (var future in futures) {
    var result = await future;
    yield result;
  }
}
