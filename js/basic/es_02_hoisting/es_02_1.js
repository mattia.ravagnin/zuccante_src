console.log("hoisting demonstration using \"var\"\n")

var x = 1;

function foo() {
  console.log(x);
  var x = 2;
  console.log(x);
}

foo();

console.log(x);

/* HOISTING

var x = 1

function foo() {
   var x;
   console.log(x); 
   x = 2;
   console.log(x); 
}

foo();

console.log(x); 

*/