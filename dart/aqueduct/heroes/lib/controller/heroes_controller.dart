import 'package:heroes/heroes.dart';
import 'package:heroes/model/hero.dart';

class HeroesController extends ResourceController {

  HeroesController(this.context);
  
  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllHeroes() async {
    final heroQuery = Query<Hero>(context);
    final heroes = await heroQuery.fetch();

    return Response.ok(heroes);
  }

  @Operation.get('id')
  Future<Response> getHeroByID(@Bind.path("id") int id) async {
    final heroQuery = Query<Hero>(context)
    ..where((h) => h.id).equalTo(id);    

    final hero = await heroQuery.fetchOne();

    if (hero == null) {
      // return Response.notFound();
      return Response.ok({"message": "not_found"});
    }
    return Response.ok(hero);
  }

  @Operation.post()
  Future<Response> createHero() async {
    final hero = Hero()
      ..read(await request.body.decode(), ignore: ["id"]);
    final query = Query<Hero>(context)..values = hero;
    final insertedHero = await query.insert();
    return Response.ok(insertedHero);
  }

  @Operation.put()
  Future<Response> updateHero() async {
    // decode body and build an hero
    final hero = Hero()
      ..read(await request.body.decode());
    // is it in DB?
    final heroQuery = Query<Hero>(context)
    ..where((h) => h.id).equalTo(hero.id);    
    final heroFound = await heroQuery.fetchOne();
    // .. no
    if (heroFound == null) {
      return Response.ok({"message": "no_update"});
    }
    // ... yes
    var query = Query<Hero>(context)
    ..values.name = hero.name
    ..where((u) => u.id).equalTo(hero.id);
    query.update();
    return Response.ok(hero);
  }

  @Operation.delete('id')
  Future<Response> delHeroByID(@Bind.path("id") int id) async {
    final heroQuery = Query<Hero>(context)
    ..where((h) => h.id).equalTo(id);    
    final hero = await heroQuery.fetchOne();
    if (hero == null) {
      // return Response.notFound();
      return Response.ok({"message": "not_found"});
    }
    await heroQuery.delete();
    return Response.ok({"message": "deleted"});
  }
}
