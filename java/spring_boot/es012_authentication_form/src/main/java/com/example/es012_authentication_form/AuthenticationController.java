package com.example.es012_authentication_form;

import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

@Controller
public class AuthenticationController {

    @GetMapping("/")
    String home(Model model) {
        return "index";
    }

    @GetMapping("/login")
    public String form(@RequestParam(name = "error", required = false) String error,
                       @RequestParam(name = "logout", required = false) String logout,
                       Model model) {
        if(error != null)
            model.addAttribute("error", error);
        if(logout != null)
            model.addAttribute("logout", logout);
        return "login";
    }

    @GetMapping("/resource")
    public String getResource() {
        return "resource";
    }







}
