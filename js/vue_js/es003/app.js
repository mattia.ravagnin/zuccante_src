const Es03 = {
    data() {
        return {
            url: "https://vuejs.org/v2/guide/events.html",
            msg: "try",
            data: 0,
            clickX: 0,
            clickY: 0,
            moveX: 0,
            moveY: 0
        }
    },
    methods: {
        mouseOver() {
            this.msg = "mouse over";
            console.log("mouse over");
        },
        mouseLeave() {
            this.msg = "mouse leave";
            console.log("mouse leave");
        },
        click(event, data) {
            this.msg = "click";
            this.data = data;
            this.clickX = event.offsetX;
            this.clickY = event.offsetY;
            console.log(event);
        },
        doubleClick(event) {
            this.msg = "double click";
            console.log(event);
        },
        move(event) {
            this.moveX = event.offsetX;
            this.moveY = event.offsetY;
            this.msg = "move";
        }
        
        
    }
}
  
Vue.createApp(Es03).mount('#es003')
  