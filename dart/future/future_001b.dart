import 'dart:async';

main() {
  Future.delayed(Duration(seconds: 0), () {
    print("from myFuture");
    return 666;
  }).then((value) {
    print(value);
  });
  print("from main()");
}
