// Promise { <pending> } .... 

promise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    resolve({
      message: "The man likes to keep his word",
      code: "aManKeepsHisWord"
    });
  }, 10 * 1000);
});
console.log(promise);
// let change timeout below
setTimeout(function() {
  console.log(promise);
} , 10 * 1000);