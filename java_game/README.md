# game tutorial

Questo [tutorial](http://zetcode.com/javagames/) si basa su Java Swing, [qui](https://docs.oracle.com/javase/tutorial/uiswing/index.html) per un tutorial introduttivo. Inoltre abbiamo preso dal tutorial di Oracle [qui](https://docs.oracle.com/javase/tutorial/uiswing/painting/).

## awt_tutorial

Per iniziare.

## animation

Il primo esempio, usa il `Timer` di *awt* (è il secondo esemppio nel tutorial, nel primo viene creata la finestra, per questo rimandiamo al tutorial di Oracle).

## animation Timer

Con un altro `Timer`.

## animation_thread

Questo usa un `Thread` efficiente in quanto gestisce in modo non più sincrono il *repaint*.