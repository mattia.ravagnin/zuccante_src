const Sequelize = require('sequelize');

var sequelize = new Sequelize('classicmodels', 'classicmodels', 'classicmodels', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, 
      min: 0, 
      idle: 10000 
    },
});

// import models

const Customer = sequelize.import(__dirname + '/models/customers');
const Employee = sequelize.import(__dirname + '/models/employees');
const Office = sequelize.import(__dirname + '/models/offices');
const OrderDetail = sequelize.import(__dirname + '/models/orderdetails');
const Order = sequelize.import(__dirname + '/models/orders');
const Payment = sequelize.import(__dirname + '/models/payments');
const ProductLine = sequelize.import(__dirname + '/models/productlines');
const Product = sequelize.import(__dirname + '/models/products');

Customer.hasMany(Order, {foreignKey: 'customerNumber'});
Order.belongsTo(Customer);


sequelize.sync().then(()=>{

    console.log('\n\n');

    sequelize.query('SELECT customerName, phone FROM customers LIMIT 10').then(([results, metadata]) => {
        console.log('\n\n### raw query 1: SELECT');
        console.log('*** resulrs: ');
        results.forEach((result) => console.log(JSON.stringify(result))); 
        console.log('\n\n');
    });

    
    sequelize.query('SELECT * FROM customers LIMIT 10', {
        model: Customer, 
        // mapToModel: true
    }).then(customers => {
        console.log("\n\n### raw query 2: using model");
        customers.forEach((customer) => {
            console.log('****************************');
            console.log(`name: ${customer.customerName}`);
            console.log(`phone: ${customer.phone}`);
        }); 
        console.log('\n\n');
    })


    Customer.findAll({
        attributes: ['customerName', 'phone', 'city'],
        limit: 10,
        include: [{
            model: Order,
            attributes: ['orderNumber', 'orderDate', 'status'],
            where: { status: 'Shipped'}
        }]
    }).then(customers => {
        console.log("\n\n### raw query 3: a join");
        customers.forEach((customer) => {
            console.log('****************************');
            console.log(`name: ${customer.customerName}`);
            console.log(`phone: ${customer.phone}`);
            console.log(`phone: ${customer.city}`);
            console.log(`orders: ${JSON.stringify(customer.orders)}`);
        }); 
    });


    




});

