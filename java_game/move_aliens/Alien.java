public class Alien extends Sprite {

    private final int INITIAL_X = 400;
    private final int ALIEN_SPEED = -1;

    public Alien(int x, int y) {
        super(x, y);
        initAlien();
    }

    private void initAlien() {
        loadImage("img/alien.png");
        getImageDimensions();
    }

    public void move() {
        if (x < 0) {
            x = INITIAL_X;
        }
        x += ALIEN_SPEED;
    }
}

